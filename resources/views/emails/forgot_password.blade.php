<p>Hi {{ $name }}</p>
<p>Please click on the link to reset your password <a href="{{ $link }}">Click here</a></p>
<p>Thank you</p>
<p>Laravel Team</p>