<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CustomAuthController;
use App\Http\Controllers\Api\ArticleController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth','namespace' => 'Api'],function(){
    Route::post('login', [CustomAuthController::class, 'login']);
    Route::post('signup', [CustomAuthController::class, 'signup']);
    Route::post('forgot-password', [CustomAuthController::class, 'forgotPassword']);
    Route::post('set-password', [CustomAuthController::class, 'setPassword']);
});
Route::group(['middleware' => ['auth:api'],'prefix' => 'articles','namespace' => 'Api'],function(){
    Route::post('list', [ArticleController::class, 'index']);
    Route::get('show/{id}', [ArticleController::class, 'show']);
    Route::post('store', [ArticleController::class, 'store']);
    Route::put('update', [ArticleController::class, 'update']);
    Route::delete('destroy', [ArticleController::class, 'destroy']);
});
