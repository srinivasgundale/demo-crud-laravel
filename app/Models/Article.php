<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Eloquent;

class Article extends Eloquent
{
    protected $table = 'articles';
    protected $fillable = [
        'name','description'
    ];
}