<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Exception;
use Log;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\SignupRequest;
use Mail;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use config;

class CustomAuthController extends Controller
{
    protected function login( LoginRequest $request ) {
        try {
            if (!auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Invalid Email or Password',
                    'errors' => null,
                    'result' => null,
                ], 200);
            } else {
                $accessToken = Auth::user()->createToken('authToken')->accessToken;
                $retUser = [
                        'name' => auth()->user()->name,
                        'email' => auth()->user()->email,
                        'access_token' => "Bearer ".$accessToken
                    ];
                return response()->json([
                        'status' => 'success',
                        'message' => 'User Login Successful',
                        'errors' => null,
                        'result' => $retUser
                    ], 200);
            }
        }catch(Exception $e){
            Log::debug('Registration failed');
            return response()->json( [
                'status' => 'fail',
                'message' => 'Something went wrong',
                'errors' => null,
                'result' => null
            ], 200 );
        }
    }

    protected function signup(SignupRequest $request) {
        try{
            $user = new User();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = $request->password;
            $result = $user->save();
            if ( $result ) {
                return response()->json( [
                    'status' => 'success',
                    'message' => 'User Signup Successfull',
                    'errors' => null,
                    'result' => [
                        "email" => $request->email
                    ]
                ], 200 );
            } 
            return response()->json( [
                'status' => 'fail',
                'message' => 'Something went wrong',
                'errors' => null,
                'result' => null
            ], 200 );
        }catch(Exception $e){
            Log::debug('Registration failed');
            return response()->json( [
                'status' => 'fail',
                'message' => 'Something went wrong',
                'errors' => null,
                'result' => null
            ], 200 );
        }
            
    }

    protected function forgotPassword (Request $request){
        try{
            $validator = Validator::make( $request->all(), [
                'email' => 'required|email|exists:users',
            ] );
            if ( $validator->fails() ){
                return response()->json( [
                    'status' => 'fail',
                    'message' => 'Please enter all the required fields correclty, '.implode(",",$validator->errors()->all()),
                    'errors' => $validator->errors()->all(),
                    'result' => null
                ], 200 );
            }else{
                try {
                    $user = User::whereEmail($request->email)->first();
                    $emailData = [
                        "name" => $user->name,
                        "subject" => "Reset your password!",
                        "link" => config('settings.FRONT_END_URL').'reset/'.Crypt::encryptString($user->email),
                    ];
                    Log::debug("Email sending start");
                    Mail::to($request->email)->send(new ForgotPassword($emailData));
                    Log::debug("Email sending end");
                    
                } catch (Exception $e) {
                    Log::debug("Email sending failed");
                }
                return response()->json( [
                    'status' => 'success',
                    'message' => 'Please check you email for verification link',
                    'errors' => null,
                    'result' => [
                        "email" => $user->email,
                        "link" => config('settings.FRONT_END_URL')
                    ]
                ], 200 );
            }
        }catch(Exception $e){
            Log::debug('Forgot password failed');
            return response()->json( [
                'status' => 'fail',
                'message' => 'Something went wrong',
                'errors' => null,
                'result' => null
            ], 200 );
        }
    }

    protected function setPassword(Request $request){
        $validator = Validator::make( $request->all(), [
            'password' => 'required|confirmed|min:6|max:50',
            'password_confirmation' => 'required|min:6|max:50',
            'link' => 'required'
        ]);
        if ( $validator->fails() ){
            return response()->json( [
                'status' => 'fail',
                'message' => 'Please enter all the required fields correclty, '.implode(",",$validator->errors()->all()),
                'errors' => $validator->errors()->all(),
                'result' => null
            ], 200 );
        }else{
            try {
                $decrypted = Crypt::decryptString($request->link);
                $user = User::whereEmail($decrypted)->first();
                if(!$user){
                    return response()->json( [
                        'status' => 'fail',
                        'message' => 'Invalid Link or Link Expired',
                        'errors' => null,
                        'result' => null
                    ], 200 );
                }
                $user->password = $request->password;
                $user->save();
                return response()->json( [
                    'status' => 'success',
                    'message' => 'Password updated Successfully, please login',
                    'errors' => null,
                    'result' => [
                        "name" => $user->name,
                        "email" => $user->email
                    ]
                ], 200 );
            } catch (DecryptException $e) {
                return response()->json( [
                    'status' => 'fail',
                    'message' => 'Invalid Link or Link Expired',
                    'errors' => null,
                    'result' => null
                ], 200 );
            }
        }
    }

}