<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Article;
use Illuminate\Support\Facades\Validator;
use Exception;
use Log;
use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\UpdateArticleRequest;

class ArticleController extends Controller
{
    protected function index(Request $request){
        $articles = Article::all();
        return response()->json( [
            'status' => 'success',
            'message' => 'Articles list',
            'errors' => null,
            'result' => $articles
        ], 200 );
    }
    protected function show(Request $request){
        $article = Article::find($request->id);
        return response()->json( [
            'status' => 'success',
            'message' => 'Article detail',
            'errors' => null,
            'result' => $article
        ], 200 );
    }
    protected function store(StoreArticleRequest $request){
        $article = new Article();
        $article->name = $request->name;
        $article->description = $request->description;
        $article->save();
        return response()->json( [
            'status' => 'success',
            'message' => 'Article created successfully',
            'errors' => null,
            'result' => $article
        ], 200 );
    }
    protected function update(UpdateArticleRequest $request){
        $article = Article::find($request->id);
        $article->name = $request->name;
        $article->description = $request->description;
        $article->save();
        return response()->json( [
            'status' => 'success',
            'message' => 'Article updated successfully',
            'errors' => null,
            'result' => $article
        ], 200 );
    }
    protected function destroy(Request $request){
        print_r($request->all());
        $articles = Article::whereIn('id',$request->ids)->delete();
        return response()->json( [
            'status' => 'success',
            'message' => 'Articles deleted Successfully',
            'errors' => null,
            'result' => true
        ], 200 );
    }
}