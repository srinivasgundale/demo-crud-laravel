<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class SignupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email|max:50',
            'password' => 'required|confirmed|min:6|max:50',
            'password_confirmation' => 'required|min:6|max:50'
        ];
    }
    protected function failedValidation(Validator $validator)
    {
        if ($this->wantsJson() || $this->ajax()) {
            throw new HttpResponseException(
                response()->json([
                    'status' => 'error',
                    'message' => 'Please enter all the required fields correctly, '.implode(",",$validator->errors()->all()),
                    'errors' => $validator->errors()->all(),
                    'result' => null
                ], 200)
            );
        }
        parent::failedValidation($validator);
    }

}
